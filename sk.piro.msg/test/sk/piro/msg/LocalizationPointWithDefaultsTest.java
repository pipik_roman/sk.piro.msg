/**
 * 
 */
package sk.piro.msg;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

/**
 * Testing all default methods
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class LocalizationPointWithDefaultsTest extends MessagesTestCase {

	private final DefaultMessages l1 = getDefaultMessages("defaults_en.properties");
	private final DefaultMessages l2 = getDefaultMessages("defaults_sk.properties");

	@Test
	public void testYesOrNo() {
		try {
			yesOrNoTest(l1);
			yesOrNoTest(l2);
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private static void yesOrNoTest(DefaultMessagesI l) {
		String yes = l.yesOrNo(true);
		String no = l.yesOrNo(false);
		assertEquals(l.yes(), yes);
		assertEquals(l.no(), no);
	}

	@Test
	public void test() {
		try {
			test(l1);
			test(l2);
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private static void test(DefaultMessagesI l) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Method[] methods = DefaultMessagesI.class.getMethods();
		for (Method method : methods) {
			Class<?> returnType = method.getReturnType();
			if (!String.class.equals(returnType)) {
				continue;
			}
			if (method.getName().equals("get") || method.getName().equals("yesOrNo")) {
				continue;
			}
			Class<?>[] params = method.getParameterTypes();
			if (params.length == 0) {
				System.out.println(method.invoke(l));
			} else if (params.length == 1) {
				System.out.println(method.invoke(l, "PARAM0"));
			} else if (params.length == 2) {
				System.out.println(method.invoke(l, "PARAM0", "PARAM2"));
			}
		}
	}

}
