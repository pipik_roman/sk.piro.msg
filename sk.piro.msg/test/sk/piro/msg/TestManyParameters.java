package sk.piro.msg;

import org.junit.Test;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class TestManyParameters extends MessagesTestCase {

	@Test
	public void test() {
		Messages messages = getMessages("test.properties");
		String msg = messages.get("manyParameters", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N");
		System.err.println(msg);
		assertEquals("ABCDEFGHIJKL", msg);
	}
}
