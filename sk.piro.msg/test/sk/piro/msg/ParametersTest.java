package sk.piro.msg;

import java.math.BigDecimal;

import sk.piro.msg.fragment.BasicParameter;
import sk.piro.msg.fragment.CustomParameter;
import sk.piro.msg.fragment.FormattedParameter;
import sk.piro.msg.fragment.IgnoredParameter;
import sk.piro.msg.fragment.ParameterI;
import sk.piro.msg.fragment.ReplacingFormattedParameter;
import sk.piro.msg.fragment.ReplacingParameter;
import sk.piro.msgParser.MessageParser;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ParametersTest extends MessagesTestCase {

	public void testBasicFragment() {
		{
			String TEMPLATE = "{0}";
			String PARAM = "XXX";
			ParameterI f = (ParameterI) MessageParser.parseFragment(0, TEMPLATE);
			assertTrue(f instanceof BasicParameter);
			assertEquals(0, f.getStart());
			assertEquals(3, f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, 0, PARAM);
			assertEquals(PARAM, sb.toString());
		}
		{
			String TEMPLATE = "More complex fragment {0}";
			String PARAM = "XXX";
			String RESULT = TEMPLATE.replace("{0}", PARAM);
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue(f instanceof BasicParameter);
			assertEquals(START, f.getStart());
			assertEquals(3, f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, START, PARAM);
			assertEquals(RESULT, sb.toString());
		}
	}

	public void testIgnoredShortFormatFragment() {
		{
			String TEMPLATE = "{0;ignore}";
			String PARAM = "XXX";
			ParameterI f = (ParameterI) MessageParser.parseFragment(0, TEMPLATE);
			assertTrue("Parameter should be IgnoredParameter, but is: " + f, f instanceof IgnoredParameter);
			assertEquals(0, f.getStart());
			assertEquals(TEMPLATE.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(true, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, 0, PARAM);
			assertEquals("", sb.toString());
		}
		{
			String TEMPLATE = "More complex fragment {0;ignore}";
			String PARAM = "XXX";
			String RESULT = TEMPLATE.replace("{0;ignore}", "");
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue(f instanceof IgnoredParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(true, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, START, PARAM);
			assertEquals(RESULT, sb.toString());
		}
	}

	public void testIgnoreFalse() {
		{
			String TEMPLATE = "{0;ignore=false}";
			String PARAM = "XXX";
			String RESULT = TEMPLATE.replace("{0;ignore=false}", "XXX");
			ParameterI f = (ParameterI) MessageParser.parseFragment(0, TEMPLATE);
			assertTrue(f instanceof CustomParameter);
			assertEquals(0, f.getStart());
			assertEquals(TEMPLATE.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, 0, PARAM);
			assertEquals(RESULT, sb.toString());
		}
		{
			String TEMPLATE = "More complex fragment {0;ignore=false}";
			String PARAM = "XXX";
			String RESULT = TEMPLATE.replace("{0;ignore=false}", "XXX");
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue(f instanceof CustomParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, START, PARAM);
			assertEquals(RESULT, sb.toString());
		}
	}

	public void testIgnoredFragment() {
		{
			String TEMPLATE = "{0;ignore=true}";
			String PARAM = "XXX";
			ParameterI f = (ParameterI) MessageParser.parseFragment(0, TEMPLATE);
			assertTrue(f instanceof IgnoredParameter);
			assertEquals(0, f.getStart());
			assertEquals(TEMPLATE.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(true, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, 0, PARAM);
			assertEquals("", sb.toString());
		}
		{
			String TEMPLATE = "More complex fragment {0;ignore=true}";
			String PARAM = "XXX";
			String RESULT = TEMPLATE.replace("{0;ignore=true}", "");
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue(f instanceof IgnoredParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(true, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, START, PARAM);
			assertEquals(RESULT, sb.toString());
		}
	}

	public void testFormattedFragment() {
		{
			String TEMPLATE = "{0;format=%.2f}";
			BigDecimal PARAM = new BigDecimal("2.5478");
			String RESULT = TEMPLATE.replace("{0;format=%.2f}", String.format("%.2f", PARAM));
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(0, TEMPLATE);
			assertTrue("Frament is not an instance of FormattedFragment, but: " + f, f instanceof FormattedParameter);
			assertEquals(0, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(true, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, 0, PARAM);
			assertEquals(RESULT, sb.toString());
		}
		{
			String TEMPLATE = "More complex fragment {0;format=%.2f}";
			BigDecimal PARAM = new BigDecimal("2.5478");
			String RESULT = TEMPLATE.replace("{0;format=%.2f}", String.format("%.2f", PARAM));
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue(f instanceof FormattedParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(true, f.isFormatted());
			assertEquals(false, f.isIgnored());
			StringBuilder sb = new StringBuilder(TEMPLATE);
			f.applyFragment(sb, START, PARAM);
			assertEquals(RESULT, sb.toString());
		}
	}

	public void testReplacingFormattedFragment() {
		{
			String TEMPLATE = "More complex fragment {0;format=%.2f;null=NIL}";
			BigDecimal PARAM = new BigDecimal("2.5478");
			String RESULT1 = TEMPLATE.replace("{0;format=%.2f;null=NIL}", String.format("%.2f", PARAM));
			String RESULT2 = TEMPLATE.replace("{0;format=%.2f;null=NIL}", "NIL");
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue("Frament is not an instance of ReplacingFormattedFragment, but: " + f, f instanceof ReplacingFormattedParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(true, f.isFormatted());
			assertEquals(false, f.isIgnored());
			{
				StringBuilder sb = new StringBuilder(TEMPLATE);
				f.applyFragment(sb, START, PARAM);
				assertEquals(RESULT1, sb.toString());
			}
			{
				StringBuilder sb = new StringBuilder(TEMPLATE);
				f.applyFragment(sb, START, null);
				assertEquals(RESULT2, sb.toString());
			}
		}
	}

	public void testReplacingFragment() {
		{
			String TEMPLATE = "More complex fragment {0;null=NIL}";
			BigDecimal PARAM = new BigDecimal("2.5478");
			String RESULT1 = TEMPLATE.replace("{0;null=NIL}", PARAM.toString());
			String RESULT2 = TEMPLATE.replace("{0;null=NIL}", "NIL");
			int START = TEMPLATE.indexOf('{');
			String FRAGMENT = TEMPLATE.substring(START);
			ParameterI f = (ParameterI) MessageParser.parseFragment(START, FRAGMENT);
			assertTrue("Frament is not an instance of ReplacingFragment, but: " + f, f instanceof ReplacingParameter);
			assertEquals(START, f.getStart());
			assertEquals(FRAGMENT.length(), f.getLength());
			assertEquals(0, f.getParameter());
			assertEquals(false, f.isFormatted());
			assertEquals(false, f.isIgnored());
			{
				StringBuilder sb = new StringBuilder(TEMPLATE);
				f.applyFragment(sb, START, PARAM);
				assertEquals(RESULT1, sb.toString());
			}
			{
				StringBuilder sb = new StringBuilder(TEMPLATE);
				f.applyFragment(sb, START, null);
				assertEquals(RESULT2, sb.toString());
			}
		}
	}
}
