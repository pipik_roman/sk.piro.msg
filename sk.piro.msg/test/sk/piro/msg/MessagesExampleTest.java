package sk.piro.msg;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Testing for example messages
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MessagesExampleTest extends MessagesTestCase {
	private final Messages mutableMessages = getMessages("messages-example.properties");

	public void test() {
		doTesting(mutableMessages.getMessage("messageWithoutFragments", 0), "Message without fragments");
		doTesting(mutableMessages.getMessage("messageWithOneFragment", 1), "Message with ONE fragment", "ONE");
		doTesting(mutableMessages.getMessage("messageWithTwoFragments", 2), "Message with ONE TWO fragments", "ONE", "TWO");
		doTesting(mutableMessages.getMessage("messageWithIgnoredFragment", 1), "Message with ignored fragment", "format c:");
		doTesting(mutableMessages.getMessage("messageWithIgnoredFragment2", 1), "Message with ignored fragment", "format c:");
		doTesting(mutableMessages.getMessage("messageWithNotIgnoredFragment", 1), "Message with I CANNOT BE IGNORED fragment", "I CANNOT BE IGNORED");
		// This could fail on different locales
		doTesting(mutableMessages.getMessage("messageWithFormatting", 1), "Message with 1,0000 fragment", new BigDecimal("1"));
		Date d = new Date();
		doTesting(mutableMessages.getMessage("messageWithDateTimeFormatting", 1), "ActualTime is: " + new SimpleDateFormat("dd.MM.yyyy EEEEE hh:mm:ss z").format(d), d);
		doTesting(mutableMessages.getMessage("messageWithReplacing", 1), "Are you ready: Ready as never", true);
		doTesting(mutableMessages.getMessage("messageWithReplacing", 1), "Are you ready: Nope", false);
		doTesting(mutableMessages.getMessage("messageWithReplacing", 1), "Are you ready: Don't know", new Object[] { null });
		doTesting(mutableMessages.getMessage("messageWithReplacing", 1), "Are you ready: USE Boolean!, not string", "");
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on  56%", 56.0);
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on 100%", 100.0);
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on   0%", (Object) null);
		doTesting(mutableMessages.getMessage("myNameIs", 2), "My name is Bc. Roman Pipik Csc", "Bc. ", " Csc");
		doTesting(mutableMessages.getMessage("myNameIs", 0), "My name is Roman Pipik");
		doTesting(mutableMessages.getMessage("messageWithManyParameters", 12), "1:A 2:B 3:C 4:D 5:E 6:F 7:G 8:H 9:I 10:J 11:K 12:L", 'A', "B", 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L');
	}

	public void doTesting(Message message, String expected, Object... params) {
		String result = message.get(mutableMessages, params);
		System.out.println(result);
		assertEquals(expected, result);
	}
}
