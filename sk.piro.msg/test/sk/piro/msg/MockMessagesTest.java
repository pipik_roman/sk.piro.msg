package sk.piro.msg;

import org.junit.Test;

public class MockMessagesTest extends MessagesTestCase{

	@Test
	public void test() {
		MessagesMock mock = new MessagesMock();
		String result;
		String key;
		key = "message";
		result = mock.get(key);
		assertEquals(key, result);
		
		result = mock.get(key,"PARAM1");
		assertEquals(key+" PARAM1", result);
		
		result = mock.get(key,"PARAM1","PARAM2");
		assertEquals(key+" PARAM1, PARAM2", result);
	}
}
