package sk.piro.msg;

import java.util.Map.Entry;

import org.junit.Test;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class PrintDebugTest extends MessagesTestCase {

	@Test
	public void test() {
		Messages messages = getMessages("test.properties");
		for (Entry<MessageInfo, Message> entry : messages.getMessages().entrySet()) {
			System.err.println(entry.getKey().getKey() + ": " + entry.getValue().toDebugString());
		}
	}
}
