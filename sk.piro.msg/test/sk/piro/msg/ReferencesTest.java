package sk.piro.msg;

import org.junit.Test;

import sk.piro.msg.fragment.ReferenceI;
import sk.piro.msgParser.MessageParser;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ReferencesTest extends MessagesTestCase {
	@Test
	public void testBasicReference() {
		String fragment = "{helloKittyRef}";
		Messages m = getMessages("test.properties");
		ReferenceI r = (ReferenceI) MessageParser.parseFragment(0, fragment);
		StringBuilder sb = new StringBuilder(fragment);
		r.applyFragment(m, sb, 0);
		System.err.println(r);
		assertEquals(m.get("helloKittyRef"), sb.toString());
		assertEquals(m.get("testRef"), m.get("helloKittyRef"));
	}

	@Test
	public void testReferenceWithPassingParameters() {
		String message = "testWithParameterPassingReference";
		Messages m = getMessages("test.properties");
		String result = m.get(message, "Roman");
		assertEquals("Hello World. Hello Roman", result);

		message = "testWithParameterPassingReference2";
		result = m.get(message, "Roman");
		assertEquals("Hello World. Hello Roman", result);
	}
}
