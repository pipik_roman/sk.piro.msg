package sk.piro.msg;

import junit.framework.TestCase;
import sk.piro.msgParser.PropertiesMessagesParser;

public class MessagesTestCase extends TestCase {

	public Messages getMessages(String string) {
		PropertiesMessagesParser parser = new PropertiesMessagesParser();
		parser.parse(MessagesTestCase.class.getResourceAsStream(string));
		return new Messages(parser.getMessages());
	}

	public DefaultMessages getDefaultMessages(String string) {
		PropertiesMessagesParser parser = new PropertiesMessagesParser();
		parser.parse(MessagesTestCase.class.getResourceAsStream(string));
		parser.parse(MessagesTestCase.class.getResourceAsStream("defaults.properties"));
		return new DefaultMessages(parser.getMessages());
	}
}
