package sk.piro.msg;

import java.math.BigDecimal;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MessagesTest extends MessagesTestCase {

	public void testInvalidMessage() {
		Messages m = getMessages("test.properties");
		try {
			String result = m.get("insertBracketForError", "}");
			fail("This should throw an error, but returned "+result+"! Maybe enable assert is not used!");
		} catch (MessageException e) {
			//
		}
	}

	public void test() {
		Messages m = getMessages("messages-example.properties");
		testSpaceAsKeyValueSeparator(m);
		test(m);
	}

	private void testSpaceAsKeyValueSeparator(Messages m) {
		doTesting(m.getMessage("@test124567890-.*", 0), "Testing ascii characters and space");
	}

	private void test(Messages mutableMessages) {
		doTesting(mutableMessages.getMessage("messageWithoutFragments", 0), "Message without fragments");
		doTesting(mutableMessages.getMessage("messageWithOneFragment", 1), "Message with ONE fragment", "ONE");
		doTesting(mutableMessages.getMessage("messageWithTwoFragments", 2), "Message with ONE TWO fragments", "ONE", "TWO");
		doTesting(mutableMessages.getMessage("messageWithIgnoredFragment", 1), "Message with ignored fragment", "format c:");
		doTesting(mutableMessages.getMessage("messageWithIgnoredFragment2", 1), "Message with ignored fragment", "format c:");
		doTesting(mutableMessages.getMessage("messageWithNotIgnoredFragment", 1), "Message with I CANNOT BE IGNORED fragment", "I CANNOT BE IGNORED");
		// This could fail on different locales
		doTesting(mutableMessages.getMessage("messageWithFormatting", 1), "Message with 1,0000 fragment", new BigDecimal("1"));
		doTesting(mutableMessages.getMessage("messageWithReplacing", 1), "Are you ready: Ready as never", true);
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on  56%", 56.0);
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on 100%", 100.0);
		doTesting(mutableMessages.getMessage("messageWithReplacingAndFormatting", 1), "Ready on   0%", (Object) null);
	}

	public void doTesting(Message message, String expected, Object... params) {
		String result = message.get(params);
		System.out.println(result);
		assertEquals(expected, result);
	}

}
