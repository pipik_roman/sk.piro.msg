package sk.piro.msg;

import org.junit.Test;

import sk.piro.msgParser.MessageParser;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MessageTest extends MessagesTestCase {
	private static final MessageParser mp = new MessageParser();

	@Test
	public void testWithoutFragments() {
		String template = "This template is without fragments";
		Message m = mp.parse(template);
		assertEquals(template, m.get());
	}

	@Test
	public void testWithOneFragment() {
		{
			String template = "This template has one fragment: {0}";
			String param1 = "";
			String expected = template.replace("{0}", param1);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
		{
			String template = "This template has one fragment: {0}";
			String param1 = "FRAGMENT";
			String expected = template.replace("{0}", param1);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
	}

	@Test
	public void testNullReplacement() {
		{
			String template = "This template has one fragment: {0}";
			String param1 = null;
			String expected = template.replace("{0}", "null");
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
		{
			String template = "This template has one fragment: {0;null=NIL}";
			String param1 = null;
			String expected = template.replace("{0;null=NIL}", "NIL");
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
	}

	@Test
	public void testWithManyFragmentsAndOneParameter() {
		{
			String template = "Score is {0}:{0}";
			String param1 = "4";
			String expected = template.replace("{0}", param1);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
		{
			String template = "Score is {0}:{0}";
			String param1 = "";
			String expected = template.replace("{0}", param1);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
	}

	@Test
	public void testSwappedParameters() {
		{
			String template = "Score is {1}:{0}";
			String param1 = "4";
			String param2 = "7";
			String expected = template.replace("{0}", param1).replace("{1}", param2);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1, param2));
		}
	}

	@Test
	public void testWithManyFragmentsAndManyParameters() {
		{
			String template = "Score is {0} {1}:{2} {3}";
			String param1 = "Barcelona";
			String param2 = "0";
			String param3 = "1";
			String param4 = "Madrid";
			String expected = template.replace("{0}", param1).replace("{1}", param2).replace("{2}", param3).replace("{3}", param4);

			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1, param2, param3, param4));
		}
	}

	@Test
	public void testInvalidFragments() {
		{
			String template = "My name is {1}";
			// String param1 = "Piotr";
			try {
				Message m = mp.parse(template);
				fail("Should have thrown on invalid parameter: " + m);
			} catch (AssertionError e) {
				//
			}
		}
	}

	@Test
	public void testMissingOneParameter() {
		{
			String template = "My name is {0}";
			// String param1 = "Piotr";
			Message m = mp.parse(template);
			try {
				m.get();
				fail("Should have thrown on missing parameter");
			} catch (MessageException e) {
				//
			}
		}
	}

	@Test
	public void testMissingManyParameters() {
		{
			String template = "My name is {0} and i live in {1}";
			String param1 = "Piotr";
			Message m = mp.parse(template);
			try {
				m.get(param1);
				fail("Should have thrown on missing parameter");
			} catch (MessageException e) {
				//
			}
		}
	}

	public void testFormattedParameter() {
		{
			String template = "Score is {0;format=%2.0f}:{0;format=%3.0f}";
			double param1 = 4;
			String expected = template.replace("{0;format=%2.0f}", " 4").replace("{0;format=%3.0f}", "  4");
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}
	}

	public void testIgnoredParameter() {
		{
			String template = "Score is INVALID DATA {0;ignore}";
			String param1 = "4";
			String expected = template.replace("{0;ignore}", "");
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1));
		}

		{
			String template = "Score is {1;ignore}{0;ignore}{2}";
			String param1 = "4";
			String param2 = "4";
			String param3 = "NOT PLAYED";
			String expected = template.replace("{0;ignore}", "").replace("{1;ignore}", "").replace("{2}", param3);
			Message m = mp.parse(template);
			assertEquals(expected, m.get(param1, param2, param3));
		}
	}
}
