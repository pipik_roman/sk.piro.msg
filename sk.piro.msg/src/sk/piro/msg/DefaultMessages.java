/**
 * 
 */
package sk.piro.msg;

import java.util.HashMap;
import java.util.Map;

/**
 * Default messages contains common messages with methods for all of them
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class DefaultMessages extends Messages implements DefaultMessagesI {

	private static final Map<String, DefaultMessages> localeToMessages = new HashMap<String, DefaultMessages>();
	private static DefaultMessages instance;

	/**
	 * Create default messages for specified locale
	 */
	public DefaultMessages(Map<MessageInfo, Message> messages) {
		super(messages);
	}

	/**
	 * Returns instance of default messages for specified locale
	 * 
	 * @param locale
	 *            locale to use
	 * @param setDefault
	 *            true if returned messages has to be used as defaults
	 * @param messages
	 *            messages used for default messages
	 * @return instance of default messages for specified locale
	 */
	public static final DefaultMessages getInstance(String locale, boolean setDefault, Map<MessageInfo, Message> messages) {
		assert !localeToMessages.containsKey(locale);
		DefaultMessages defaultMessages = new DefaultMessages(messages);
		localeToMessages.put(locale, defaultMessages);
		if (setDefault) {
			instance = defaultMessages;
		}
		return defaultMessages;
	}

	/**
	 * Set default instance of {@link DefaultMessages}
	 * 
	 * @param instance
	 *            instance to use
	 */
	public static final void setDefaultInstance(DefaultMessages instance) {
		assert instance != null : "Cannot set default instance of DefaultMessages";
		DefaultMessages.instance = instance;
	}

	/**
	 * Returns default instance of {@link DefaultMessages}
	 * 
	 * @return default instance of {@link DefaultMessages}
	 */
	public static final DefaultMessages getDefaultInstance() {
		assert instance != null : "No default instance is set";
		return DefaultMessages.instance;
	}

	@Override
	public String euro() {
		return get("Euro");
	}

	@Override
	public String yes() {
		return get("Yes");
	}

	@Override
	public String no() {
		return get("No");
	}

	@Override
	public String yesOrNo(boolean b) {
		if (b) {
			return yes();
		} else {
			return no();
		}
	}

	@Override
	public String ok() {
		return get("Ok");
	}

	@Override
	public String criticalError() {
		return get("CriticalError");
	}

	@Override
	public String error() {
		return get("Error");
	}

	@Override
	public String information() {
		return get("Information");
	}

	@Override
	public String options() {
		return get("Options");
	}

	@Override
	public String question() {
		return get("Question");
	}

	@Override
	public String settings() {
		return get("Settings");
	}

	@Override
	public String warning() {
		return get("Warning");
	}

	@Override
	public String help() {
		return get("Help");
	}

	@Override
	public String aboutApplication() {
		return get("AboutApplication");
	}

	@Override
	public String version() {
		return get("Version");
	}

	@Override
	public String waitMsg() {
		return get("Wait");
	}

	@Override
	public String cancel() {
		return get("Cancel", "");
	}

	@Override
	public String cancel(String what) {
		return get("Cancel", " " + what);
	}

	@Override
	public String canceling() {
		return get("Canceling", "");
	}

	@Override
	public String canceling(String what) {
		return get("Canceling", " " + what);
	}

	@Override
	public String close() {
		return get("Close", "");
	}

	@Override
	public String close(String what) {
		return get("Close", " " + what);
	}

	@Override
	public String open() {
		return get("Open", "");
	}

	@Override
	public String open(String what) {
		return get("Open", " " + what);
	}

	@Override
	public String edit() {
		return get("Edit", "");
	}

	@Override
	public String edit(String what) {
		return get("Edit", " " + what);
	}

	@Override
	public String remove() {
		return get("Remove", "");
	}

	@Override
	public String remove(String what) {
		return get("Remove", " " + what);
	}

	@Override
	public String delete() {
		return get("Delete", "");
	}

	@Override
	public String delete(String what) {
		return get("Delete", " " + what);
	}

	@Override
	public String hide() {
		return get("Hide", "");
	}

	@Override
	public String hide(String what) {
		return get("Hide", " " + what);
	}

	@Override
	public String show() {
		return get("Show", "");
	}

	@Override
	public String show(String what) {
		return get("Show", " " + what);
	}

	@Override
	public String search() {
		return get("Search", "");
	}

	@Override
	public String search(String what) {
		return get("Search", " " + what);
	}

	@Override
	public String confirm() {
		return get("Confirm", "");
	}

	@Override
	public String confirm(String what) {
		return get("Confirm", " " + what);
	}

	@Override
	public String stop() {
		return get("Stop", "");
	}

	@Override
	public String stop(String what) {
		return get("Stop", " " + what);
	}

	@Override
	public String download() {
		return get("Download", "");
	}

	@Override
	public String download(String what) {
		return get("Download", " " + what);
	}

	@Override
	public String upload() {
		return get("Upload", "");
	}

	@Override
	public String upload(String what) {
		return get("Upload", " " + what);
	}

	@Override
	public String update() {
		return get("Update", "");
	}

	@Override
	public String update(String what) {
		return get("Update", " " + what);
	}

	@Override
	public String downloading() {
		return get("Downloading", "");
	}

	@Override
	public String downloading(String what) {
		return get("Downloading", " " + what);
	}

	@Override
	public String uploading() {
		return get("Uploading", "");
	}

	@Override
	public String uploading(String what) {
		return get("Uploading", " " + what);
	}

	@Override
	public String updating() {
		return get("Updating", "");
	}

	@Override
	public String updating(String what) {
		return get("Updating", " " + what);
	}

	@Override
	public String up() {
		return get("Up");
	}

	@Override
	public String down() {
		return get("Down");
	}

	@Override
	public String left() {
		return get("Left");
	}

	@Override
	public String right() {
		return get("Right");
	}

	@Override
	public String center() {
		return get("Center");
	}

	@Override
	public String above() {
		return get("Above", "");
	}

	@Override
	public String above(String what) {
		return get("Above", " " + what);
	}

	@Override
	public String under() {
		return get("Under", "");
	}

	@Override
	public String under(String what) {
		return get("Under", " " + what);
	}

	@Override
	public String before() {
		return get("Before", "");
	}

	@Override
	public String behind() {
		return get("Behind", "");
	}

	@Override
	public String behind(String what) {
		return get("Behind", " " + what);
	}

	@Override
	public String over() {
		return get("Over", "");
	}

	@Override
	public String over(String what) {
		return get("Over", " " + what);
	}

	@Override
	public String after() {
		return get("After", "");
	}

	@Override
	public String after(String what) {
		return get("After", " " + what);
	}

	@Override
	public String north() {
		return get("North");
	}

	@Override
	public String south() {
		return get("South");
	}

	@Override
	public String east() {
		return get("East");
	}

	@Override
	public String west() {
		return get("West");
	}

	@Override
	public String southEast() {
		return get("SouthEast");
	}

	@Override
	public String southWest() {
		return get("SouthWest");
	}

	@Override
	public String northWest() {
		return get("NorthWest");
	}

	@Override
	public String northEast() {
		return get("NorthEast");
	}

	@Override
	public String date() {
		return get("Date", "");
	}

	@Override
	public String dateAndTime() {
		return get("DateAndTime", "");
	}

	@Override
	public String time() {
		return get("Time", "");
	}

	@Override
	public String timeAndDate() {
		return get("TimeAndDate", "");
	}

	@Override
	public String date(String ofWhat) {
		return get("Date", " " + ofWhat);
	}

	@Override
	public String dateAndTime(String ofWhat) {
		return get("DateAndTime", " " + ofWhat);
	}

	@Override
	public String time(String ofWhat) {
		return get("Time", " " + ofWhat);
	}

	@Override
	public String timeAndDate(String ofWhat) {
		return get("TimeAndDate", " " + ofWhat);
	}

	@Override
	public String monday() {
		return get("Monday");
	}

	@Override
	public String tuesday() {
		return get("Tuesday");
	}

	@Override
	public String wednesday() {
		return get("Wednesday");
	}

	@Override
	public String thursday() {
		return get("Thursday");
	}

	@Override
	public String friday() {
		return get("Friday");
	}

	@Override
	public String saturday() {
		return get("Saturday");
	}

	@Override
	public String sunday() {
		return get("Sunday");
	}

	@Override
	public String weekend() {
		return get("Weekend");
	}

	@Override
	public String workday() {
		return get("Workday");
	}

	@Override
	public String workdays() {
		return get("Workdays");
	}

	@Override
	public String days() {
		return get("Days");
	}

	@Override
	public String hour() {
		return get("Hour");
	}

	@Override
	public String hours() {
		return get("Hours");
	}

	@Override
	public String minute() {
		return get("Minute");
	}

	@Override
	public String minutes() {
		return get("Minutes");
	}

	@Override
	public String month() {
		return get("Month");
	}

	@Override
	public String months() {
		return get("Months");
	}

	@Override
	public String second() {
		return get("Second");
	}

	@Override
	public String seconds() {
		return get("Seconds");
	}

	@Override
	public String week() {
		return get("Week");
	}

	@Override
	public String weeks() {
		return get("Weeks");
	}

	@Override
	public String year() {
		return get("Year");
	}

	@Override
	public String years() {
		return get("Years");
	}

	@Override
	public String morning() {
		return get("Morning");
	}

	@Override
	public String day() {
		return get("Day");
	}

	@Override
	public String noon() {
		return get("Noon");
	}

	@Override
	public String afternoon() {
		return get("Afternoon");
	}

	@Override
	public String evening() {
		return get("Evening");
	}

	@Override
	public String night() {
		return get("Night");
	}

	@Override
	public String midnight() {
		return get("Midnight");
	}

	@Override
	public String tiny() {
		return get("Tiny");
	}

	@Override
	public String small() {
		return get("Small");
	}

	@Override
	public String medium() {
		return get("Medium");
	}

	@Override
	public String large() {
		return get("Large");
	}

	@Override
	public String extraLarge() {
		return get("ExtraLarge");
	}

	@Override
	public String longMsg() {
		return get("Long");
	}

	@Override
	public String shortMsg() {
		return get("Short");
	}

	@Override
	public String wide() {
		return get("Wide");
	}

	@Override
	public String narrow() {
		return get("Narrow");
	}

	@Override
	public String black() {
		return get("Black");
	}

	@Override
	public String blue() {
		return get("Blue");
	}

	@Override
	public String brightness() {
		return get("Brightness");
	}

	@Override
	public String brown() {
		return get("Brown");
	}

	@Override
	public String cyan() {
		return get("Cyan");
	}

	@Override
	public String darkGray() {
		return get("DarkGray");
	}

	@Override
	public String gray() {
		return get("Gray");
	}

	@Override
	public String green() {
		return get("Green");
	}

	@Override
	public String hue() {
		return get("Hue");
	}

	@Override
	public String lightGray() {
		return get("LightGray");
	}

	@Override
	public String magenta() {
		return get("Magenta");
	}

	@Override
	public String orange() {
		return get("Orange");
	}

	@Override
	public String pink() {
		return get("Pink");
	}

	@Override
	public String red() {
		return get("Red");
	}

	@Override
	public String saturation() {
		return get("Saturation");
	}

	@Override
	public String transparency() {
		return get("Transparency");
	}

	@Override
	public String transparent() {
		return get("Transparent");
	}

	@Override
	public String white() {
		return get("White");
	}

	@Override
	public String yellow() {
		return get("Yellow");
	}

	@Override
	public String address() {
		return get("Address");
	}

	@Override
	public String age() {
		return get("Age");
	}

	@Override
	public String all() {
		return get("All");
	}

	@Override
	public String city() {
		return get("City");
	}

	@Override
	public String closing() {
		return get("Closing", "");
	}

	@Override
	public String closing(String what) {
		return get("Closing", " " + what);
	}

	@Override
	public String confirming() {
		return get("Confirming", "");
	}

	@Override
	public String confirming(String what) {
		return get("Confirming", " " + what);
	}

	@Override
	public String create() {
		return get("Create", "");
	}

	@Override
	public String create(String what) {
		return get("Create", " " + what);
	}

	@Override
	public String creating() {
		return get("Creating", "");
	}

	@Override
	public String creating(String what) {
		return get("Creating", " " + what);
	}

	@Override
	public String dateOfBirth() {
		return get("DateOfBirth");
	}

	@Override
	public String deleting() {
		return get("Deleting", "");
	}

	@Override
	public String deleting(String what) {
		return get("Deleting", " " + what);
	}

	@Override
	public String description() {
		return get("Description");
	}

	@Override
	public String editing() {
		return get("Editing", "");
	}

	@Override
	public String editing(String what) {
		return get("Editing", " " + what);
	}

	@Override
	public String email() {
		return get("Email");
	}

	@Override
	public String failedTo(String what) {
		return get("FailedTo", " " + what);
	}

	@Override
	public String fax() {
		return get("Fax");
	}

	@Override
	public String generate() {
		return get("Generate", "");
	}

	@Override
	public String generate(String what) {
		return get("Generate", " " + what);
	}

	@Override
	public String generating() {
		return get("Generating", "");
	}

	@Override
	public String generating(String what) {
		return get("Generating", " " + what);
	}

	@Override
	public String hiding() {
		return get("Hiding", "");
	}

	@Override
	public String hiding(String what) {
		return get("Hiding", " " + what);
	}

	@Override
	public String image() {
		return get("Image");
	}

	@Override
	public String justify() {
		return get("Justify");
	}

	@Override
	public String justifyCenter() {
		return get("JustifyCenter");
	}

	@Override
	public String justifyLeft() {
		return get("JustifyLeft");
	}

	@Override
	public String justifyRight() {
		return get("JustifyRight");
	}

	@Override
	public String layer() {
		return get("Layer");
	}

	@Override
	public String layers() {
		return get("Layers");
	}

	@Override
	public String load() {
		return get("Load", "");
	}

	@Override
	public String load(String what) {
		return get("Load", " " + what);
	}

	@Override
	public String loading() {
		return get("Loading", "");
	}

	@Override
	public String loading(String what) {
		return get("Loading", " " + what);
	}

	@Override
	public String name() {
		return get("Name");
	}

	@Override
	public String opening() {
		return get("Opening", "");
	}

	@Override
	public String opening(String what) {
		return get("Opening", " " + what);
	}

	@Override
	public String phone() {
		return get("Phone");
	}

	@Override
	public String reload() {
		return get("Reload", "");
	}

	@Override
	public String reload(String what) {
		return get("Reload", " " + what);
	}

	@Override
	public String reloading() {
		return get("Reloading", "");
	}

	@Override
	public String reloading(String what) {
		return get("Reloading", " " + what);
	}

	@Override
	public String removing() {
		return get("Removing", "");
	}

	@Override
	public String removing(String what) {
		return get("Removing", " " + what);
	}

	@Override
	public String save() {
		return get("Save", "");
	}

	@Override
	public String save(String what) {
		return get("Save", " " + what);
	}

	@Override
	public String saving() {
		return get("Saving", "");
	}

	@Override
	public String saving(String what) {
		return get("Saving", " " + what);
	}

	@Override
	public String searching() {
		return get("Searching", "");
	}

	@Override
	public String searching(String what) {
		return get("Searching", " " + what);
	}

	@Override
	public String showing() {
		return get("Showing", "");
	}

	@Override
	public String showing(String what) {
		return get("Showing", " " + what);
	}

	@Override
	public String stopping() {
		return get("Stopping", "");
	}

	@Override
	public String stopping(String what) {
		return get("Stopping", " " + what);
	}

	@Override
	public String street() {
		return get("Street");
	}

	@Override
	public String streetNumber() {
		return get("StreetNumber");
	}

	@Override
	public String surname() {
		return get("Surname");
	}

	@Override
	public String text() {
		return get("Text");
	}

	@Override
	public String textFont() {
		return get("TextFont");
	}

	@Override
	public String textSize() {
		return get("TextSize");
	}

	@Override
	public String textStyle() {
		return get("TextStyle");
	}

	@Override
	public String title() {
		return get("Title");
	}

	@Override
	public String titleAfter() {
		return get("TitleAfter");
	}

	@Override
	public String titleBefore() {
		return get("TitleBefore");
	}

	@Override
	public String validate() {
		return get("Validate", "");
	}

	@Override
	public String validate(String what) {
		return get("Validate", " " + what);
	}

	@Override
	public String validating() {
		return get("Validating", "");
	}

	@Override
	public String validating(String what) {
		return get("Validating", " " + what);
	}

	@Override
	public String waiting() {
		return get("Waiting", "");
	}

	@Override
	public String waiting(String what) {
		return get("Waiting", " " + what);
	}

	@Override
	public String zip() {
		return get("Zip");
	}

	@Override
	public String zoom() {
		return get("Zoom");
	}

	@Override
	public String zoomIn() {
		return get("ZoomIn");
	}

	@Override
	public String zoomOut() {
		return get("ZoomOut");
	}

	@Override
	public String confirmEmail() {
		return get("ConfirmEmail");
	}

	@Override
	public String confirmPassword() {
		return get("ConfirmPassword");
	}

	@Override
	public String login() {
		return get("Login");
	}

	@Override
	public String password() {
		return get("Password");
	}

	@Override
	public String showCharacters() {
		return get("ShowCharacters");
	}

	@Override
	public String symbolEuro() {
		return get("Symbol.Euro");
	}

	@Override
	public String symbolDegree() {
		return get("Symbol.Degree");
	}

	@Override
	public String language() {
		return get("Language");
	}

	@Override
	public String languageName() {
		return get("LanguageName");
	}

	@Override
	public String application() {
		return get("Application");
	}

	@Override
	public String program() {
		return get("Program");
	}
}
