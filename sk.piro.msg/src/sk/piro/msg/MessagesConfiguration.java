package sk.piro.msg;

import java.io.Serializable;
/**
 *	Configuration class for {@link Messages} 
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 *
 */
public class MessagesConfiguration implements Serializable{
	private static final long serialVersionUID = 1L;
	private boolean storeHandlerMessages;
	private MessageNotFoundHandler messageNotFoundHandler;
	
	private MessagesConfiguration() {
		// for GWT serialization
	}
	
	public MessagesConfiguration(boolean storeHandlerMessages, MessageNotFoundHandler messageNotFoundHandler) {
		this();
		this.storeHandlerMessages = storeHandlerMessages;
		this.messageNotFoundHandler = messageNotFoundHandler;
	}
	
	/**
	 * Returns true if messages from handlers should be stored for reuse (Use more memory, but is faster)
	 * @return true if messages from handlers should be stored for reuse (Use more memory, but is faster)
	 */
	public boolean storeHandlerMessages() {
		return storeHandlerMessages;
	}
	/**
	 * Returns handler used when message is not found
	 * @return handler used when message is not found
	 */
	public MessageNotFoundHandler getMessageNotFoundHandler() {
		return messageNotFoundHandler;
	}
}
