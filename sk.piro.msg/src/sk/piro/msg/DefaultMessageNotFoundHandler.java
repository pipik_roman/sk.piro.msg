package sk.piro.msg;

import java.util.ArrayList;
import java.util.List;

import sk.piro.msg.fragment.BasicParameter;
import sk.piro.msg.fragment.FragmentI;

/**
 * Implementation of {@link MessageNotFoundHandler} returning default message to use. 
 * You can override {@link #createTemplate(String, MessageInfo)} to localize it. 
 * Also {@link #createParamString(MessageInfo)} and {@link #createFragments(String, MessageInfo)} can be overridden to customize
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 */
public class DefaultMessageNotFoundHandler implements MessageNotFoundHandler {
	
	private static final long serialVersionUID = 1L;

	@Override
	public Message onMessageNotFound(MessageInfo info) throws MessageException {
		String paramString = createParamString(info);
		String template = createTemplate(paramString,info);
		List<FragmentI> fragments = createFragments(template, info);
		return new Message(template, info.getParamCount(), fragments);
	}

	public String createParamString(MessageInfo info) {
		StringBuilder sb = new StringBuilder("");
		if(info.getParamCount() > 0) {
			sb.append(' ');
		}
		for(int i = 0; i < info.getParamCount(); i++) {
			if(i == 0) {
				sb.append("{"+i+"}");
			}
			else{
				sb.append(", {"+i+"}");
			}
		}
		return sb.toString();
	}

	public List<FragmentI> createFragments(String template, MessageInfo info) {
		if(info.getParamCount() == 0) {
			return null;
		}
		List<FragmentI> fragments = new ArrayList<FragmentI>(info.getParamCount());
		int start = -1;
		for(int i = 0;i < info.getParamCount();i++) {
			String paramString = "{"+i+"}";
			start = template.indexOf(paramString,start+1);
			if(start == -1) {
				throw new IllegalArgumentException("Cannot resolve parameter start for "+paramString+" for template "+ template);
			}
			fragments.add(new BasicParameter(start, i));
		}
		return fragments;
	}

	public String createTemplate(String paramString, MessageInfo info) {
		return info.getKey() + paramString;
	}
}
