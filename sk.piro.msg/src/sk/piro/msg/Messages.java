package sk.piro.msg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import sk.piro.msg.fragment.FragmentI;

/**
 * Immutable implementation of messages
 * 
 * @author Ing. Pipik Roman - roman.pipik@dominanz.sk pipik.roman@gmail.com www.dominanz.sk
 * 
 */
public class Messages implements MessagesI {
	private static final long serialVersionUID = 1L;
	/**
	 * Default configuration to use. Uses {@link ThrowingMessageNotFoundHandler}
	 */
	public static final MessagesConfiguration DEFAULT_CONFIGURATION = new MessagesConfiguration(false, new ThrowingMessageNotFoundHandler());

	final public static boolean isValidKeyword(String keyword) {
		return keyword.matches(FragmentI.REGEX_KEYWORD);
	}
	// End of static part
	
	protected Map<MessageInfo, Message> messages;
	protected MessagesConfiguration configuration;

	private Messages() {
		// for GWT serialization
	}
	
	/**
	 * Create messages from specified message map and default configuration. Default configuration is {@link #DEFAULT_CONFIGURATION}
	 * @param messages message map to use
	 */
	public Messages(Map<MessageInfo, Message> messages) {
		this(messages,DEFAULT_CONFIGURATION);
	}
	
	/**
	 * Create messages from specified message map and configuration
	 * @param messages message map to use
	 * @param configuration configuration
	 */
	public Messages(Map<MessageInfo,Message> messages, MessagesConfiguration configuration) {
		this();
		if(messages == null) {
			throw new NullPointerException("Map<MessageInfo,Message> messages cannot be null!");
		}
		if(configuration == null) {
			throw new NullPointerException("MessagesConfiguration configuration cannot be null!");
		}
		this.messages = messages;
		this.configuration = configuration;
	}

	@Override
	final public MessagesConfiguration getConfiguration() {
		return configuration;
	}
	
	@Override
	public boolean hasMessage(String key) {
		return hasMessage(key, 0);
	}

	@Override
	public boolean hasMessage(String key, int paramCount) {
		return hasMessage(new MessageInfo(key, paramCount));
	}

	@Override
	public boolean hasMessage(MessageInfo info) {
		return messages.containsKey(info);
	}

	@Override
	public Message getMessage(String key, int paramCount) {
		return getMessage(new MessageInfo(key, paramCount));
	}

	@Override
	public Message getMessage(MessageInfo info) {
		return messages.get(info);
	}

	@Override
	public String get(String key) throws MessageException {
		return get(new MessageInfo(key, 0));
	}

	@Override
	public String get(MessageInfo info, Object... objects) throws MessageException {
		Message message = getMessage(info);
		if(message == null) {
			message = getConfiguration().getMessageNotFoundHandler().onMessageNotFound(info);
			if(getConfiguration().storeHandlerMessages()) {
				messages.put(info, message);
			}
		}
		return message.get(this, objects);
	}

	@Override
	public String get(String key, Object... objects) throws MessageException {
		return get(new MessageInfo(key, objects.length), objects);
	}

	@Override
	public boolean containsAll(Iterable<MessageInfo> requiredMessages) throws AssertionError {
		if (requiredMessages == null) {
			return true;
		}
		List<MessageInfo> list = new ArrayList<MessageInfo>();
		for (MessageInfo info : requiredMessages) {
			if (!this.messages.containsKey(info)) {
				list.add(info);
			}
		}
		if (list.isEmpty()) {
			return true;
		} else {
			throw new AssertionError("This messages doesn't contain all required messages. Missing messages: \n" + list);
		}
	}

	/**
	 * Returns a copy of map with messages
	 */
	@Override
	public Map<MessageInfo, Message> getMessages() {
		return new HashMap<MessageInfo, Message>(messages);
	}

	@Override
	public Iterator<MessageInfo> iterator() {
		return messages.keySet().iterator();
	}
}
