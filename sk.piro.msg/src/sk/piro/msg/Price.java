/**
 * 
 */
package sk.piro.msg;

import java.math.BigDecimal;

/**
 * Price class is used to render numbers as price. Price has two digits scale
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class Price {

	private BigDecimal price;

	/**
	 * Create price
	 * 
	 * @param price
	 *            price to use
	 */
	public Price(BigDecimal price) {
		this.price = price.setScale(2);
	}

	@Override
	public String toString() {
		return price.toString() + DefaultMessages.getDefaultInstance().euro();
	}
}
