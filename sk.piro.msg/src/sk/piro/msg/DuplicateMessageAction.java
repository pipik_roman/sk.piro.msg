package sk.piro.msg;

public enum DuplicateMessageAction {
	/**
	 * Overwrite original message
	 */
	Overwrite,
	/**
	 * Throw exception
	 */
	Throw,
	/**
	 * Throw exception only when content of message is different (same name, same parameter count, but different content!)
	 */
	ThrowIfDifferent
}
