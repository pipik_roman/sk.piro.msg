/**
 * 
 */
package sk.piro.msg;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import sk.piro.msg.fragment.FragmentI;

/**
 * Mutable representation of messages
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MutableMessages extends Messages implements MutableMessagesI {

	private static final long serialVersionUID = 1L;
	private DuplicateMessageAction duplicateMessageAction;

	final public static void assertIsValidKeyword(String key) {
		assert isValidKeyword(key) : "Keyword '" + key + "' is not valid. Keys has to be matching regex " + FragmentI.REGEX_KEYWORD;
	}

	protected MutableMessages(DuplicateMessageAction duplicateMessageAction, Map<MessageInfo, Message> messages) throws MessageException {
		super(messages);
		this.duplicateMessageAction = duplicateMessageAction;
	}

	public MutableMessages(DuplicateMessageAction duplicateMessageAction) throws MessageException {
		this(duplicateMessageAction, new HashMap<MessageInfo, Message>());
	}

	private void checkNewMessage(MessageInfo info, Message message) {
		int params = info.getParamCount();
		if (hasMessage(info)) {
			Message oldMessage = getMessage(info);
			if (message.equals(oldMessage) && getDuplicateMessageAction() == DuplicateMessageAction.Throw) {
				String text = "Messages already contains message with key: '" + info.getKey() + "' and parameter count: " + params + " (with same message template)! \nContained message: '"
						+ oldMessage + "',\n message to add: '" + message + "'";
				throw new AssertionError(text);
			} else if (getDuplicateMessageAction() == DuplicateMessageAction.ThrowIfDifferent) {
				String text = "Messages already contains message with key: '" + info.getKey() + "' and parameter count: " + params + " (with different message template) !\nContained message: '"
						+ oldMessage + "',\n message to add: '" + message + "'";
				throw new AssertionError(text);
			}
		}
	}

	@Override
	public void add(String key, Message message) {
		add(new MessageInfo(key, message.getParamCount()), message);
	}

	@Override
	public void add(Map<MessageInfo, Message> messages) {
		for (Entry<MessageInfo, Message> entry : messages.entrySet()) {
			add(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void add(MessageInfo messageInfo, Message message) {
		if (getDuplicateMessageAction() != DuplicateMessageAction.Overwrite) {
			checkNewMessage(messageInfo, message);
		}
		assertIsValidKeyword(messageInfo.getKey());
		getMessages().put(messageInfo, message);
	}

	@Override
	public DuplicateMessageAction getDuplicateMessageAction() {
		return duplicateMessageAction;
	}

	@Override
	public void setDuplicateMessageAction(DuplicateMessageAction duplicateMessageAction) {
		this.duplicateMessageAction = duplicateMessageAction;
	}

	/**
	 * Returns map of messages backing this messages. Any changes into map will be reflected in this object
	 */
	@Override
	public Map<MessageInfo, Message> getMessages() {
		return this.messages;
	}
}
