package sk.piro.msg;

import java.util.HashMap;

/**
 * Empty messages with {@link DefaultMessageNotFoundHandler} so messages are always resolved.
 * @author Ing. Pipík Roman, roman.pipik@dominanz.sk, pipik.roman@gmail.com
 *
 */
public class MessagesMock extends Messages {
	
	private static final long serialVersionUID = 1L;

	public MessagesMock() {
		super(new HashMap<MessageInfo, Message>(), new MessagesConfiguration(false, new DefaultMessageNotFoundHandler()));
	}
	
}
