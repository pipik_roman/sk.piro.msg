/**
 * 
 */
package sk.piro.msg;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
interface DefaultMessagesI extends MessagesI {
	/**
	 * Returns localized word 'Language'
	 * 
	 * @return localized word 'Language'
	 */
	String language();

	/**
	 * Returns localized name of specified language
	 * 
	 * @return localized name of specified language
	 */
	String languageName();

	String application();

	String program();

	String symbolEuro();

	String symbolDegree();

	String euro();

	String yes();

	String no();

	/**
	 * Returns yes localization when true, no localization when false
	 * 
	 * @param b
	 *            logical state to localize
	 * @return yes localization when true, no localization when false
	 */
	String yesOrNo(boolean b);

	String cancel();

	String cancel(String what);

	String canceling();

	String canceling(String what);

	String ok();

	String criticalError();

	String error();

	String warning();

	String information();

	String question();

	String options();

	String settings();

	String help();

	String aboutApplication();

	String version();

	String waitMsg();

	String close();

	String close(String what);

	String open();

	String open(String what);

	String edit();

	String edit(String what);

	String remove();

	String remove(String what);

	String delete();

	String delete(String what);

	String hide();

	String hide(String what);

	String show();

	String show(String what);

	String search();

	String search(String what);

	String confirm();

	String confirm(String what);

	String upload();

	String upload(String what);

	String download();

	String download(String what);

	String update();

	String update(String what);

	String uploading();

	String uploading(String what);

	String downloading();

	String downloading(String what);

	String updating();

	String updating(String what);

	String stop();

	String stop(String what);

	String validate();

	String validate(String what);

	String load();

	String load(String what);

	String save();

	String save(String what);

	String create();

	String create(String what);

	String generate();

	String generate(String what);

	String reload();

	String reload(String what);

	String waiting();

	String waiting(String what);

	String closing();

	String closing(String what);

	String opening();

	String opening(String what);

	String editing();

	String editing(String what);

	String removing();

	String removing(String what);

	String deleting();

	String deleting(String what);

	String hiding();

	String hiding(String what);

	String showing();

	String showing(String what);

	String searching();

	String searching(String what);

	String confirming();

	String confirming(String what);

	String stopping();

	String stopping(String what);

	String validating();

	String validating(String what);

	String loading();

	String loading(String what);

	String saving();

	String saving(String what);

	String creating();

	String creating(String what);

	String generating();

	String generating(String what);

	String reloading();

	String reloading(String what);

	String layer();

	String layers();

	String image();

	String description();

	String all();

	String text();

	String textSize();

	String textFont();

	String textStyle();

	String justify();

	String justifyLeft();

	String justifyRight();

	String justifyCenter();

	String zoom();

	String zoomIn();

	String zoomOut();

	String failedTo(String what);

	String email();

	String address();

	String city();

	String zip();

	String street();

	String streetNumber();

	String fax();

	String phone();

	String name();

	String surname();

	String title();

	String titleBefore();

	String titleAfter();

	String age();

	String dateOfBirth();

	String login();

	String password();

	String confirmPassword();

	String confirmEmail();

	String showCharacters();

	String up();

	String down();

	String left();

	String right();

	String center();

	String above();

	String above(String what);

	String under();

	String under(String what);

	String before();

	String behind();

	String behind(String what);

	String over();

	String over(String what);

	String after();

	String after(String what);

	String north();

	String south();

	String east();

	String west();

	String southEast();

	String southWest();

	String northWest();

	String northEast();

	String date();

	String time();

	String dateAndTime();

	String timeAndDate();

	String date(String ofWhat);

	String time(String ofWhat);

	String dateAndTime(String ofWhat);

	String timeAndDate(String ofWhat);

	String monday();

	String tuesday();

	String wednesday();

	String thursday();

	String friday();

	String saturday();

	String sunday();

	String weekend();

	String workday();

	String workdays();

	String year();

	String month();

	String week();

	String hour();

	String minute();

	String second();

	String years();

	String months();

	String weeks();

	String days();

	String hours();

	String minutes();

	String seconds();

	String morning();

	String day();

	String noon();

	String afternoon();

	String evening();

	String night();

	String midnight();

	String tiny();

	String small();

	String medium();

	String large();

	String extraLarge();

	String longMsg();

	String shortMsg();

	String wide();

	String narrow();

	String black();

	String blue();

	String brown();

	String cyan();

	String darkGray();

	String gray();

	String green();

	String lightGray();

	String magenta();

	String orange();

	String pink();

	String red();

	String transparency();

	String transparent();

	String white();

	String yellow();

	String hue();

	String saturation();

	String brightness();
}
