/**
 * 
 */
package sk.piro.msg;

import java.io.Serializable;

/**
 * Represents informations about message. It contains message key and number of parameters. Can be used to find message with specific key and number of parameters
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MessageInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String key;
	private int paramCount;

	protected MessageInfo() {
		// for serialization
	}

	/**
	 * Used to create info for message without parameters
	 * 
	 * @param key
	 *            key to use
	 */
	public MessageInfo(String key) {
		this(key, 0);
	}

	/**
	 * Create message info
	 * 
	 * @param key
	 *            message key
	 * @param paramCount
	 *            number of parameters
	 */
	public MessageInfo(String key, int paramCount) {
		this.key = key;
		this.paramCount = paramCount;
	}

	public String getKey() {
		return key;
	}

	public int getParamCount() {
		return paramCount;
	}

	/**
	 * Used for exact matching even with parameters!
	 * 
	 * @param messageInfo
	 *            message info to compare
	 * @return true if informations are same, even with parameters
	 */
	public boolean equals(MessageInfo messageInfo) {
		return this.key.equals(messageInfo.key) && this.paramCount == messageInfo.paramCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + paramCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MessageInfo)) {
			return false;
		}
		MessageInfo other = (MessageInfo) obj;
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		} else if (paramCount != other.paramCount) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return key + "(" + paramCount + ")";
	}

}
