package sk.piro.msg;

public class ThrowingMessageNotFoundHandler implements MessageNotFoundHandler {

	private static final long serialVersionUID = 1L;

	@Override
	public Message onMessageNotFound(MessageInfo info) throws MessageException {
		throw new MessageException(info, null);
	}

}
