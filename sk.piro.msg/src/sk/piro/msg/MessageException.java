/**
 * 
 */
package sk.piro.msg;

import java.util.Arrays;

/**
 * Exception thrown in Localization API
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MessageException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * Create exception associated with message, exception and objects
	 * 
	 * @param message
	 *            message
	 * @param t
	 *            exception
	 * @param objects
	 *            objects
	 */
	public MessageException(Message message, Throwable t, Object... objects) {
		super("Message cannot be localized: " + message.getTemplate() + ", parameters: " + Arrays.toString(objects), t);
	}
	
	public MessageException(MessageInfo info, Throwable t, Object ... objects) {
		super("Message "+ info+" cannot be localized, parameters: "+ Arrays.toString(objects),t);
	}

	/**
	 * Localization exception created from other cause
	 * 
	 * @param t
	 *            cause
	 */
	public MessageException(Throwable t) {
		super(t);
	}

	/**
	 * Exception with description only
	 * 
	 * @param description
	 *            description
	 */
	public MessageException(String description) {
		super(description);
	}
}
