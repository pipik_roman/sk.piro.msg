package sk.piro.msg;

import java.io.Serializable;

public interface MessageNotFoundHandler extends Serializable {
	/**
	 * Invoked when message is not found for specified message info
	 * @param info info about message not found
	 * @return fallback message to use
	 * @throws MessageException if cannot return fallback message
	 */
	public Message onMessageNotFound(MessageInfo info) throws MessageException;
}
