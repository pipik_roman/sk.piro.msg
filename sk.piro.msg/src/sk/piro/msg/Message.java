/**
 * 
 */
package sk.piro.msg;

import java.io.Serializable;
import java.util.List;

import sk.piro.msg.fragment.FragmentI;
import sk.piro.msg.fragment.ParameterI;
import sk.piro.msg.fragment.ReferenceI;

/**
 * Message is text with fragments to replace. Fragments are referencing concrete parameters by index. Parameters has to be specified when transforming message from template. Number of fragments >=
 * number of parameters. Fragment can reference one parameter many times, but every parameter has to be referenced at least once. Fragments can specify when parameter should be ignored, how it is
 * formatted and replace parameter with more meaningful value.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	private String template;
	private int paramCount;
	private List<FragmentI> fragmentIs;

	private  Message() {
		// for GWT serialization
	}

	public Message(String template, int paramCount, List<FragmentI> fragmentIs) {
		this();
		this.template = template;
		this.paramCount = paramCount;
		this.fragmentIs = fragmentIs;
	}

	public String getTemplate() {
		return template;
	}

	public int getFragmentCount() {
		if (this.fragmentIs == null) {
			return 0;
		}
		return fragmentIs.size();
	}

	public int getParamCount() {
		return this.paramCount;
	}

	/**
	 * Returns string created from message. Message cannot contain any parameters since no parameters are provided!.
	 * 
	 * @return String created from message
	 * @throws MessageException
	 *             if message cannot be translated
	 */
	public String get() throws MessageException {
		return this.get((MutableMessages) null);
	}

	/**
	 * Returns string created by replacing message fragments with parameters. Message cannot contain any references because this method doesn't provide any messages to reference
	 * 
	 * @param objects
	 *            object as parameters
	 * @return string created by replacing message fragments with parameters
	 * @throws MessageException
	 *             if message cannot be translated
	 */
	public String get(Object... objects) throws MessageException {
		return this.get(null, objects);
	}

	/**
	 * Return string with replaced fragments.
	 * 
	 * @param messages
	 *            messages to reference,can be null if there are no references to other messages
	 * @param parameters
	 *            objects to replace fragments
	 * @return resulting string
	 * @throws MessageException
	 *             if message cannot be translated
	 */
	public String get(MessagesI messages, Object... parameters) throws MessageException {
		try {
			assert paramCount == parameters.length : "Message '" + this + "' requires " + paramCount + " parameters, but " + parameters.length
					+ "where given! Did this message parameters changed recently?";
			if (fragmentIs == null) {
				return template;
			}
			int drift = 0;// drift specifies where next fragment starts
			StringBuilder sb = new StringBuilder(template);
			for (FragmentI f : fragmentIs) {
				int driftedStart = drift + f.getStart();
				int appliedSize;
				if (f.isParameter()) {
					ParameterI p = (ParameterI) f;
					appliedSize = p.applyFragment(sb, driftedStart, parameters[p.getParameter()]);
				} else {
					ReferenceI r = (ReferenceI) f;
					appliedSize = r.applyFragment(messages, sb, driftedStart, parameters);
				}

				drift = drift - f.getLength() + appliedSize;
			}
			String result = sb.toString();
			assert noFragment(result) : "Result of message cannot contain fragment text ({reference} or {0} ...). This can indicate error in fragment parsing. Result: " + result;
			return result;
		} catch (Throwable t) {
			throw new MessageException(this, t, parameters);
		}
	}

	private static boolean noFragment(String result) {
		return !result.matches(FragmentI.REGEX_PARAMETER_OR_REFERENCE);
	}

	public List<FragmentI> getFragments() {
		return fragmentIs;
	}

	/**
	 * Returns template of this message
	 */
	@Override
	public String toString() {
		return template;
	}

	/**
	 * Returns debug representation of message. It should look similar to template but it is created from parsed fragments
	 * 
	 * @return debug representation of message
	 */
	public String toDebugString() {
		try {
			if (fragmentIs == null) {
				return template;
			}
			int drift = 0;// drift specifies where next fragment starts
			StringBuilder sb = new StringBuilder(template);
			for (FragmentI f : fragmentIs) {
				int driftedStart = drift + f.getStart();
				int appliedSize = f.applyDebugString(sb, driftedStart);
				drift = drift - f.getLength() + appliedSize;
			}
			return sb.toString();
		} catch (Throwable t) {
			throw new MessageException(this, t);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((template == null) ? 0 : template.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Message)) {
			return false;
		}
		Message other = (Message) obj;
		if (template == null) {
			if (other.template != null) {
				return false;
			}
		} else if (!template.equals(other.template)) {
			return false;
		}
		return true;
	}
}
