package sk.piro.msg.fragment;

import sk.piro.msg.MessageException;

/**
 * Abstract implementation of fragment. Fragments are compared and equals by parameter
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class AbstractParameter extends AbstractFragment implements ParameterI, Comparable<AbstractParameter> {
	private static final long serialVersionUID = 1L;

	protected int parameter;

	protected AbstractParameter() {
		super();
		// for serialization
	}

	public AbstractParameter(int start, int parameter) {
		super(start);
		this.parameter = parameter;
	}

	@Override
	public int getParameter() {
		return parameter;
	}

	@Override
	public boolean isIgnored() {
		return false;
	}

	@Override
	public boolean isFormatted() {
		return false;
	}

	@Override
	public String getFormat() {
		throw new IllegalStateException("BasicFragment doesn't have format");
	}

	/**
	 * Fragments are compared by parameter
	 */
	@Override
	public int compareTo(AbstractParameter o) {
		return this.parameter - o.parameter;
	}

	@Override
	final public int applyFragment(StringBuilder builder, int start, Object parameter) {
		String s = parseParameter(parameter);
		builder.replace(start, start + getLength(), s);
		return s.length();
	}

	protected String parseParameter(Object parameter) {
		if (parameter == null) {
			return getReplacementFor(null);
		} else {
			return parameter.toString();
		}
	}

	@Override
	public String getReplacementFor(Object object) {
		if (object == null) {
			return "null";
		} else
			throw new MessageException("No supported replacements for " + object + " in fragment: " + toString());
	}

	/**
	 * Hash code of fragment is based on parameter
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + parameter;
		return result;
	}

	/**
	 * Fragments are equal by parameter
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractParameter)) {
			return false;
		}
		AbstractParameter other = (AbstractParameter) obj;
		if (parameter != other.parameter) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isParameter() {
		return true;
	}

	@Override
	public boolean isReference() {
		return false;
	}

}
