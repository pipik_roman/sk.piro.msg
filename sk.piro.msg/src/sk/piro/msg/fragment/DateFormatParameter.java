package sk.piro.msg.fragment;

import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class DateFormatParameter extends FormattedParameter {
	private static final long serialVersionUID = 1L;
	private Format dateFormat;

	protected DateFormatParameter() {
		super();
		// for serialization
	}

	/**
	 * Create fragment with format
	 * 
	 * @param start
	 *            start of fragment
	 * @param param
	 *            parameter index
	 * @param length
	 *            length of fragment
	 * @param format
	 *            format to use
	 */
	public DateFormatParameter(int start, int param, int length, String format) {
		super(start, param, length, format);
		dateFormat = new SimpleDateFormat(format);
	}

	@Override
	public boolean isFormatted() {
		return true;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	protected String parseParameter(Object parameter) {
		if (parameter == null) {
			return "null";
		} else {
			return dateFormat.format(parameter);
		}
	}

	@Override
	public String toString() {
		return "DateFormattedParameter [format=" + format + ", length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}
}
