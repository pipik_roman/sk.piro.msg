package sk.piro.msg.fragment;

/**
 * Basic fragment - not ignored, has no format
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class BasicParameter extends AbstractParameter {

	private static final long serialVersionUID = 1L;

	protected BasicParameter() {
		super();
		// for serialization
	}

	/**
	 * Create basic fragment
	 * 
	 * @param start
	 *            start of fragment
	 * @param parameter
	 *            parameter index for fragment
	 */
	public BasicParameter(int start, int parameter) {
		super(start, parameter);
	}

	@Override
	public int getLength() {
		return Integer.toString(getParameter()).length() + 2;
	}

	@Override
	public String toString() {
		return "BasicParameter [parameter=" + parameter + ", start=" + start + "]";
	}
}
