package sk.piro.msg.fragment;

import java.util.Map;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ReplacingFormattedParameter extends ReplacingParameter {
	private static final long serialVersionUID = 1L;
	protected String format;

	protected ReplacingFormattedParameter() {
		super();
		// for serialization
	}

	/**
	 * @param start
	 *            start of fragment
	 * @param parameter
	 *            parameter index
	 * @param length
	 *            length of fragment
	 * @param replacements
	 *            replacements to use
	 * @param format
	 *            format to use
	 */
	public ReplacingFormattedParameter(int start, int parameter, int length, Map<String, String> replacements, String format) {
		super(start, parameter, length, replacements);
		this.format = format;
	}

	@Override
	protected String parseParameter(Object parameter) {
		if (parameter == null) {
			return getReplacementFor(null);
		}
		String replacement = getReplacementFor(parameter);
		if (replacement != null) {
			return replacement;
		} else {
			return String.format(format, parameter);
		}
	}

	@Override
	public boolean isFormatted() {
		return true;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	public String toString() {
		return "ReplacingFormattedParameter [format=" + format + ", replacements=" + replacements + ", length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}

}
