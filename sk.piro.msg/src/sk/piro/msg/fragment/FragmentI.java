package sk.piro.msg.fragment;

import java.io.Serializable;

/**
 * Contains data about fragment
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface FragmentI extends Serializable {
	public static final String REGEX_KEYWORD = "[@a-zA-Z_\\.*-][@a-zA-Z0-9_\\.*-]*";
	public static final String REGEX_REFERENCE = "\\{" + REGEX_KEYWORD + "(;[0-9]+)*\\}";
	public static final String REGEX_PARAMETER = "\\{[0-9]+(()|(;ignore)|(;[a-zA-Z]+=[^}]+))\\}";
	public static final String REGEX_PARAMETER_OR_REFERENCE = "(" + REGEX_PARAMETER + ")|(" + REGEX_REFERENCE + ")";

	/**
	 * Returns start position of fragment
	 * 
	 * @return start position of fragment
	 */
	public int getStart();

	/**
	 * Returns length of fragment
	 * 
	 * @return length of fragment
	 */
	public int getLength();

	/**
	 * Returns true if fragment is parameter. If true getParameter will return valid index of parameter to use
	 * 
	 * @return true if fragment is parameter. If true getParameter will return valid index of parameter to use
	 */
	public boolean isParameter();

	/**
	 * Returns true if fragment is reference
	 * 
	 * @return true if fragment is reference
	 */
	public boolean isReference();

	/**
	 * Replace fragment with debug string of fragment
	 * 
	 * @param sb
	 *            string builder to use
	 * @param driftedStart
	 *            start of fragment
	 * @return applied size
	 */
	public int applyDebugString(StringBuilder sb, int driftedStart);
}
