package sk.piro.msg.fragment;

/**
 * Fragment with format
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class FormattedParameter extends CustomParameter {
	private static final long serialVersionUID = 1L;
	protected String format;

	protected FormattedParameter() {
		super();
		// for serialization
	}

	/**
	 * Create fragment with format
	 * 
	 * @param start
	 *            start of fragment
	 * @param param
	 *            parameter index
	 * @param length
	 *            length of fragment
	 * @param format
	 *            format to use
	 */
	public FormattedParameter(int start, int param, int length, String format) {
		super(start, param, length);
		this.format = format;
	}

	@Override
	public boolean isFormatted() {
		return true;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	protected String parseParameter(Object parameter) {
		if (parameter == null) {
			return "null";
		} else {
			return String.format(format, parameter);
		}
	}

	@Override
	public String toString() {
		return "FormattedParameter [format=" + format + ", length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}

}
