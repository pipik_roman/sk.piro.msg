package sk.piro.msg.fragment;

/**
 * Ignored fragment ignores format and specified parameter
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class IgnoredParameter extends CustomParameter {

	private static final long serialVersionUID = 1L;

	protected IgnoredParameter() {
		super();
		// for serialization
	}

	/**
	 * Create ignored fragment
	 * 
	 * @param start
	 *            start index of fragment
	 * @param parameter
	 *            parameter to ignore
	 * @param length
	 *            length of fragment
	 */
	public IgnoredParameter(int start, int parameter, int length) {
		super(start, parameter, length);
	}

	@Override
	public boolean isIgnored() {
		return true;
	}

	@Override
	protected String parseParameter(Object parameter) {
		return "";
	}

	@Override
	public String toString() {
		return "IgnoredParameter [length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}

}
