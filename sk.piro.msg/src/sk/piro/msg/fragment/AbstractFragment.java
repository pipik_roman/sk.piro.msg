package sk.piro.msg.fragment;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class AbstractFragment implements FragmentI {

	private static final long serialVersionUID = 1L;

	protected int start;

	protected AbstractFragment() {
		// for serialization
	}

	/**
	 * Create abstract fragment with specified start of fragment
	 * 
	 * @param start
	 *            index of start
	 */
	public AbstractFragment(int start) {
		this.start = start;
	}

	@Override
	public int getStart() {
		return start;
	}

	@Override
	public int applyDebugString(StringBuilder sb, int start) {
		String s = toString();
		sb.replace(start, start + getLength(), s);
		return s.length();
	}
}
