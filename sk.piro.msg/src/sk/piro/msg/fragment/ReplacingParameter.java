package sk.piro.msg.fragment;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import sk.piro.msg.MessageException;

/**
 * Basic fragment that is able to replace provided parameter for other value if it matches. Currently supporting Boolean 'true','false', 'null','empty' for empty string
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ReplacingParameter extends CustomParameter {
	private static final long serialVersionUID = 1L;
	protected Map<Object, String> replacements = new HashMap<Object, String>();

	public ReplacingParameter() {
		super();
		// for serialization
	}

	/**
	 * @param start
	 *            start of fragment
	 * @param parameter
	 *            parameter index
	 * @param length
	 *            length of fragment
	 * @param replacements
	 *            replacements to use
	 */
	public ReplacingParameter(int start, int parameter, int length, Map<String, String> replacements) {
		super(start, parameter, length);
		processReplacements(replacements);
	}

	private void processReplacements(Map<String, String> replacements) {
		for (Entry<String, String> entry : replacements.entrySet()) {
			String key = entry.getKey();
			Object keyObject;
			if ("true".equals(key)) {
				keyObject = Boolean.TRUE;
			} else if ("false".equals(key)) {
				keyObject = Boolean.FALSE;
			} else if ("null".equals(key)) {
				keyObject = null;
			} else if ("empty".equals(key)) {
				keyObject = "";
			} else {
				throw new MessageException("FragmentWithReplacement doesn't support replacement for: '" + entry + "'");
			}
			this.replacements.put(keyObject, entry.getValue());
		}
		if (!this.replacements.containsKey(null)) {
			this.replacements.put(null, "null");
		}
	}

	@Override
	public String getReplacementFor(Object o) {
		return replacements.get(o);
	}

	@Override
	protected String parseParameter(Object parameter) {
		if (parameter == null) {
			return getReplacementFor(null);
		}
		String replacement = getReplacementFor(parameter);
		if (replacement != null) {
			return replacement;
		} else {
			return parameter.toString();
		}
	}

	@Override
	public String toString() {
		return "ReplacingParameter [replacements=" + replacements + ", length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}
}
