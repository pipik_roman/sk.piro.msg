package sk.piro.msg.fragment;

/**
 * Reference to other message
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class BasicReference extends AbstractReference {
	private static final long serialVersionUID = 1L;
	private static final int[] EMPTY_PARAMETERS = new int[] {};

	protected BasicReference() {
		super();
		// for serialization
	}

	/**
	 * Create basic reference
	 * 
	 * @param start
	 *            index of start
	 * @param messageRef
	 *            message to reference
	 */
	public BasicReference(int start, String messageRef) {
		super(start, messageRef);
	}

	@Override
	public int getLength() {
		return messageRef.length() + 2;
	}

	@Override
	public int[] getParameters() {
		return EMPTY_PARAMETERS;
	}

	@Override
	public String toString() {
		return "BasicReference [messageRef=" + messageRef + ", start=" + start + "]";
	}
}
