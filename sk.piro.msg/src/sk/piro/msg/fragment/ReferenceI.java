package sk.piro.msg.fragment;

import sk.piro.msg.MessagesI;

/**
 * Subtype of {@link FragmentI} that references other message
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ReferenceI extends FragmentI {
	/**
	 * Apply fragment to {@link StringBuilder}
	 * 
	 * @param mutableMessages
	 *            messages to reference, can be null if this is parameter fragment
	 * @param builder
	 *            builder to use
	 * @param start
	 *            start of this fragment
	 * @param parameters
	 *            parameters to pass parameters to referenced message
	 * @return size of applied string
	 */
	public int applyFragment(MessagesI mutableMessages, StringBuilder builder, int start, Object... parameters);

	/**
	 * Returns used parameter numbers
	 * 
	 * @return used parameter numbers
	 */
	public int[] getParameters();
}
