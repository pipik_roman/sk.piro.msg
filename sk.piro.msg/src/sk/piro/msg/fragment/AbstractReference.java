package sk.piro.msg.fragment;

import sk.piro.msg.Messages;
import sk.piro.msg.MessagesI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class AbstractReference extends AbstractFragment implements ReferenceI {
	private static final long serialVersionUID = 1L;

	protected String messageRef;

	protected AbstractReference() {
		super();
		// for serialization
	}

	/**
	 * Create abstract reference
	 * 
	 * @param start
	 *            index of start
	 * @param messageRef
	 *            message to reference
	 */
	public AbstractReference(int start, String messageRef) {
		super(start);
		assert Messages.isValidKeyword(messageRef) : "Message reference keyword '" + messageRef + "' is not valid keyword. Keywords has to match regex " + REGEX_KEYWORD;
		this.messageRef = messageRef;
	}

	@Override
	public boolean isParameter() {
		return false;
	}

	@Override
	public boolean isReference() {
		return true;
	}

	@Override
	final public int applyFragment(MessagesI mutableMessages, StringBuilder builder, int start, Object... parameters) {
		String s = getMessage(mutableMessages, messageRef, parameters);
		builder.replace(start, start + getLength(), s);
		return s.length();
	}

	protected String getMessage(MessagesI messages, String messageRef, @SuppressWarnings("unused") Object... parameters) {
		//TODO parameters are not used to find reference?! What if reference has parameters?
		//assert messages.hasMessage(messageRef, 0) : "Message '" + messageRef + "' not found in messages. Cannot reference this message!";
		return messages.get(messageRef);//FIXME cannot call without parameter count!
	}

	public static boolean isReference(String frag) {
		return REGEX_REFERENCE.matches(frag);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((messageRef == null) ? 0 : messageRef.hashCode());
		return result;
	}

	/**
	 * References are equal by messageRef
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractReference)) {
			return false;
		}
		AbstractReference other = (AbstractReference) obj;
		if (messageRef == null) {
			if (other.messageRef != null) {
				return false;
			}
		} else if (!messageRef.equals(other.messageRef)) {
			return false;
		}
		return true;
	}
}
