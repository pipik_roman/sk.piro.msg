package sk.piro.msg.fragment;

import java.util.Arrays;
import java.util.List;

import sk.piro.msg.MessagesI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ParameterPassingReference extends BasicReference {

	private static final long serialVersionUID = 1L;
	private int[] paramIndexes;
	private int length;

	protected ParameterPassingReference() {
		super();
		// for serialization
	}

	/**
	 * @param start
	 *            start of fragment
	 * @param refName
	 *            reference name
	 * @param length
	 *            length of fragment
	 * @param paramIndexes
	 *            indexes of parameters
	 */
	public ParameterPassingReference(int start, String refName, int length, List<Integer> paramIndexes) {
		super(start, refName);
		this.length = length;
		this.paramIndexes = new int[paramIndexes.size()];
		for (int i = 0; i < this.paramIndexes.length; i++) {
			this.paramIndexes[i] = paramIndexes.get(i);
		}
	}

	@Override
	public int getLength() {
		return this.length;
	}

	@Override
	protected String getMessage(MessagesI mutableMessages, String messageRef, Object... parameters) {
		assert paramIndexes.length <= parameters.length : "Reference: " + this + " passes " + paramIndexes.length + " parameters, but only: " + parameters.length + " provided. ParameterIndexes: "
				+ Arrays.toString(paramIndexes) + ", parameters: " + Arrays.toString(parameters);
		assert mutableMessages.hasMessage(messageRef, paramIndexes.length) : "Message '" + messageRef + "' not found in messages. Cannot reference this message!";
		Object[] refParams = new Object[paramIndexes.length];
		for (int i = 0; i < paramIndexes.length; i++) {
			refParams[i] = parameters[paramIndexes[i]];
		}
		return mutableMessages.get(messageRef, refParams);
	}

	@Override
	public int[] getParameters() {
		return paramIndexes;
	}

	@Override
	public String toString() {
		return "ParameterPassingReference [paramIndexes=" + Arrays.toString(paramIndexes) + ", length=" + length + ", messageRef=" + messageRef + ", start=" + start + "]";
	}

}
