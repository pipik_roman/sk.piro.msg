package sk.piro.msg.fragment;

/**
 * Subtype of {@link FragmentI}. It uses parameter provided by user to replace fragment
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ParameterI extends FragmentI {
	/**
	 * Returns true if fragment is ignored
	 * 
	 * @return true if fragment is ignored
	 */
	public boolean isIgnored();

	/**
	 * Returns true if fragment has format
	 * 
	 * @return true if fragment has format
	 */
	public boolean isFormatted();

	/**
	 * Returns format or throws {@link IllegalStateException} if there is no format
	 * 
	 * @return format
	 * @throws IllegalStateException
	 *             if there is no format
	 */
	public String getFormat() throws IllegalStateException;

	/**
	 * Returns parameter index
	 * 
	 * @return parameter index
	 */
	public int getParameter();

	/**
	 * Apply fragment to {@link StringBuilder}
	 * 
	 * @param builder
	 *            builder to use
	 * @param start
	 *            start of this fragment
	 * @param parameter
	 *            parameter to use in fragment
	 * @return size of applied string
	 */
	public int applyFragment(StringBuilder builder, int start, Object parameter);

	/**
	 * Returns value to replace specified value, or null if replacement is not specified or supported
	 * 
	 * @param object
	 *            object to replace
	 * @return value to replace specified value, or null if replacement is not specified or supported
	 */
	public String getReplacementFor(Object object);
}
