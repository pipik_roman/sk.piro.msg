package sk.piro.msg.fragment;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CustomParameter extends AbstractParameter {

	private static final long serialVersionUID = 1L;
	protected int length;

	protected CustomParameter() {
		super();
		// for serialization
	}

	/**
	 * @param start
	 *            start of fragment
	 * @param parameter
	 *            parameter index
	 * @param length
	 *            length of fragment in template
	 */
	public CustomParameter(int start, int parameter, int length) {
		super(start, parameter);
		this.length = length;
	}

	@Override
	public int getLength() {
		return length;
	}

	@Override
	public String toString() {
		return "CustomParameter [length=" + length + ", parameter=" + parameter + ", start=" + start + "]";
	}

}
