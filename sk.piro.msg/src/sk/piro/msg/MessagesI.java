package sk.piro.msg;

import java.io.Serializable;
import java.util.Map;

public interface MessagesI extends Iterable<MessageInfo>, Serializable{

	/**
	 * Returns configuration for messages
	 * @return configuration for messages
	 */
	public MessagesConfiguration getConfiguration();
	
	/**
	 * Returns messages mapped by {@link MessageInfo}. Messages can have same key, but they differ depending on number of parameters
	 * 
	 * @return messages mapped by {@link MessageInfo}. Messages can have same key, but they differ depending on number of parameters
	 */
	public Map<MessageInfo, Message> getMessages();

	/**
	 * Returns true if there is message for specified key with no parameters
	 * 
	 * @param key
	 *            key to search for
	 * @return true if there is message for specified key with no parameters
	 */
	public boolean hasMessage(String key);

	/**
	 * Returns true if there is message for specified key
	 * 
	 * @param key
	 *            key to search for
	 * @param paramCount
	 *            parameter count
	 * @return true if there is message for specified key
	 */
	public boolean hasMessage(String key, int paramCount);

	/**
	 * Returns true if there is message for specified key and with specified number of parameters
	 * 
	 * @param info
	 *            info containing key and paramCount informations
	 * @return true if there is message for specified key and with specified number of parameters
	 */
	public boolean hasMessage(MessageInfo info);

	/**
	 * Returns message for key. Functions set for messages are not used in message!
	 * 
	 * @param key
	 *            key for message
	 * @param paramCount
	 *            number of parameters
	 * @return message
	 * @throws AssertionError
	 *             if there is no such message
	 */
	public Message getMessage(String key, int paramCount) throws AssertionError;

	/**
	 * Returns message for info.
	 * 
	 * @param info
	 *            info for message
	 * @return message
	 * @throws AssertionError
	 *             if there is no such message
	 */
	public Message getMessage(MessageInfo info) throws AssertionError;

	/**
	 * Returns string created from message with no parameters (can contain references without parameters)
	 * 
	 * @param key
	 *            key of message
	 * @return final String of message
	 * @throws MessageException
	 *             if there is no such message, or message cannot be translated
	 */
	public String get(String key) throws MessageException;

	/**
	 * Returns string created from message and fragments.
	 * 
	 * @param info
	 *            info of message
	 * @param objects
	 *            message parameters
	 * @return final String of message
	 * @throws MessageException
	 *             if there is no such message, or message cannot be translated
	 */
	public String get(MessageInfo info, Object... objects) throws MessageException;

	/**
	 * Returns string created from message and fragments.
	 * 
	 * @param key
	 *            key of message
	 * @param objects
	 *            message parameters
	 * @return final String of message
	 * @throws MessageException
	 *             if there is no such message, or message cannot be translated
	 */
	public String get(String key, Object... objects) throws MessageException;

	/**
	 * Returns true if all specified messages are contained in this messages
	 * 
	 * @param requiredMessages
	 *            required messages, or null to ignore
	 * @return true if all specified messages are contained in this messages
	 * @throws AssertionError
	 *             if there is required message missing with description of missing message
	 */
	public boolean containsAll(Iterable<MessageInfo> requiredMessages) throws AssertionError;
}
