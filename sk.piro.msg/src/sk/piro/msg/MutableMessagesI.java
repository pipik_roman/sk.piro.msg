/**
 * 
 */
package sk.piro.msg;

import java.util.Map;

/**
 * Interface for messages
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface MutableMessagesI extends MessagesI {
	/**
	 * Adds message to this messages. Auxiliary method for {@link #add(MessageInfo, Message)}
	 * 
	 * @param key
	 *            key of message
	 * @param message
	 *            message
	 */
	public void add(String key, Message message);

	/**
	 * Adds message to this messages.
	 * 
	 * @param messageInfo
	 *            messageInfo of message (key, number of params)
	 * @param message
	 *            message
	 */
	public void add(MessageInfo messageInfo, Message message);

	/**
	 * Adds all messages in map
	 * 
	 * @param messages
	 *            messages
	 */
	public void add(Map<MessageInfo, Message> messages);

	/**
	 * Returns action to perform on duplicate message
	 * 
	 * @return action to perform on duplicate message
	 */
	public DuplicateMessageAction getDuplicateMessageAction();

	/**
	 * Set action to perform on duplicate message
	 * 
	 * @param duplicateMessageAction
	 *            action to perform on duplicate message
	 */
	public void setDuplicateMessageAction(DuplicateMessageAction duplicateMessageAction);
}
