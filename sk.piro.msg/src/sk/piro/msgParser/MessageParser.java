package sk.piro.msgParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sk.piro.msg.Message;
import sk.piro.msg.MessageException;
import sk.piro.msg.fragment.BasicParameter;
import sk.piro.msg.fragment.BasicReference;
import sk.piro.msg.fragment.CustomParameter;
import sk.piro.msg.fragment.DateFormatParameter;
import sk.piro.msg.fragment.FormattedParameter;
import sk.piro.msg.fragment.FragmentI;
import sk.piro.msg.fragment.IgnoredParameter;
import sk.piro.msg.fragment.ParameterI;
import sk.piro.msg.fragment.ParameterPassingReference;
import sk.piro.msg.fragment.ReferenceI;
import sk.piro.msg.fragment.ReplacingFormattedParameter;
import sk.piro.msg.fragment.ReplacingParameter;

public class MessageParser {

	private static final String STRING_IGNORE = "ignore";
	private static final String STRING_FORMAT = "format";
	private static final String DATE_FORMAT = "date";
	private static final String REGEX_IGNORE = ";ignore\\}";
	private static final String REGEX_PARAM = ";[^;\\}]+=[^;\\}]*";
	private static final String REGEX_PARAM_NUMBER = "[0-9]+";
	private static final String REGEX_PARAM_NUMBER_IN_REFERENCES = "[0-9]+";
	private static final Pattern PATTERN_PARAM = Pattern.compile(REGEX_PARAM);
	private static final Pattern PATTERN_IGNORE = Pattern.compile(REGEX_IGNORE);
	private static final Pattern PATTERN_PARAM_NUMBER = Pattern.compile(REGEX_PARAM_NUMBER);
	private static final Pattern PATTERN_PARAM_NUMBER_IN_REFERENCES = Pattern.compile(REGEX_PARAM_NUMBER_IN_REFERENCES);
	private static final Pattern PATTERN_PARAMETER_OR_REFERENCE = Pattern.compile(FragmentI.REGEX_PARAMETER_OR_REFERENCE);
	private static final Pattern PATTERN_REFERENCE = Pattern.compile(FragmentI.REGEX_REFERENCE);

	public Message parse(String template) {
		final List<FragmentI> fragmentIs = parseTemplate(template);
		final int paramCount = countParams(template, fragmentIs);
		return new Message(template, paramCount, fragmentIs);
	}

	private static List<FragmentI> parseTemplate(String template) {
		Matcher matcher = PATTERN_PARAMETER_OR_REFERENCE.matcher(template);
		List<FragmentI> fragmentIs = null;
		if (matcher.find()) {
			fragmentIs = new ArrayList<FragmentI>();
			do {
				int start = matcher.start();
				int end = matcher.end();
				FragmentI f = parseFragment(start, template.substring(start, end));
				fragmentIs.add(f);
			} while (matcher.find());
		}
		return fragmentIs;
	}

	public static FragmentI parseFragment(int start, String rawFragment) {
		int length = rawFragment.length();
		String fragmentBody = rawFragment.substring(1, length - 1);
		if (isReference(rawFragment)) {
			return processReference(start, length, fragmentBody);
		}

		int parameterIndex;
		try {
			Matcher m = PATTERN_PARAM_NUMBER.matcher(fragmentBody);
			boolean found = m.find();
			assert found : "Parameter index has to be found for Parameter";
			parameterIndex = Integer.parseInt(m.group());
		} catch (NumberFormatException e) {
			throw new MessageException("Cannot parse parameter number from fragment: " + rawFragment);
		}

		if (PATTERN_IGNORE.matcher(rawFragment).find()) {
			return new IgnoredParameter(start, parameterIndex, length);
		}

		Map<String, String> params = null;
		Matcher matcher = PATTERN_PARAM.matcher(fragmentBody);
		if (matcher.find()) {
			params = new HashMap<String, String>();
			do {
				String param = matcher.group();
				int i = param.indexOf('=');
				params.put(param.substring(1, i), param.substring(i + 1, param.length()));
			} while (matcher.find());
		}
		if (params == null) {
			return new BasicParameter(start, parameterIndex);
		} else if (isIgnoring(params)) {
			assert params.isEmpty() : "Ignored fragment should not have specified other parameters!See: " + rawFragment;
			return new IgnoredParameter(start, parameterIndex, length);
		} else if (isFormatting(params)) {
			String format = params.remove(STRING_FORMAT);
			if (params.isEmpty()) {// formatting without replacing
				return new FormattedParameter(start, parameterIndex, length, format);
			} else {
				return new ReplacingFormattedParameter(start, parameterIndex, length, params, format);
			}
		} else if (isDateFormatting(params)) {
			String format = params.remove(DATE_FORMAT);
			return new DateFormatParameter(start, parameterIndex, length, format);
		} else if (isReplacing(params)) {
			return new ReplacingParameter(start, parameterIndex, length, params);
		} else {
			assert params.isEmpty() : "No parameters expected here! They should be removed in process of parsing. Parameters: " + params;
			return new CustomParameter(start, parameterIndex, length);
		}
	}

	private static boolean isReference(String fragmentBody) {
		return PATTERN_REFERENCE.matcher(fragmentBody).matches();
	}

	private static boolean isFormatting(Map<String, String> params) {
		return params.containsKey(STRING_FORMAT);
	}

	private static boolean isIgnoring(Map<String, String> params) {
		String s = params.remove(STRING_IGNORE);
		return "true".equals(s);
	}

	private static boolean isReplacing(Map<String, String> params) {
		return params.size() > 0;
	}

	private static int countParams(String template, List<FragmentI> fragmentIs) {
		if (fragmentIs == null) {
			return 0;
		} else {
			HashSet<Integer> set = new HashSet<Integer>();
			for (FragmentI f : fragmentIs) {
				if (f.isParameter()) {
					set.add(((ParameterI) f).getParameter());
				} else if (f.isReference()) {
					for (int pi : ((ReferenceI) f).getParameters()) {
						set.add(pi);
					}
				}
			}
			assert assertParametersValid(set) : "Invalid parameters for template " + template + ", are you missing parameter in template? Fragments=" + fragmentIs + ",paramCount=" + set.size()
					+ " Parameters has to start with 0 up to 9. Parameters cannot be ignored!";
			return set.size();
		}
	}

	private static boolean isDateFormatting(Map<String, String> params) {
		return params.containsKey(DATE_FORMAT);
	}

	private static FragmentI processReference(int start, int length, String fragmentBody) {
		int refNameEnd = fragmentBody.indexOf(';');
		if (refNameEnd == -1) {
			return new BasicReference(start, fragmentBody);
		} else {
			String refName = fragmentBody.substring(0, refNameEnd);
			String rest = fragmentBody.substring(refNameEnd);
			Matcher matcher = PATTERN_PARAM_NUMBER_IN_REFERENCES.matcher(rest);
			List<Integer> paramIndexes = null;
			if (matcher.find()) {
				paramIndexes = new ArrayList<Integer>();
				do {
					paramIndexes.add(Integer.parseInt(matcher.group()));
				} while (matcher.find());
			}
			if (paramIndexes != null && !paramIndexes.isEmpty()) {
				return new ParameterPassingReference(start, refName, length, paramIndexes);
			} else {
				throw new AssertionError("No parameter indexes parsed for fragment: " + fragmentBody + ", probably bad syntax");
			}
		}
	}

	private static boolean assertParametersValid(HashSet<Integer> set) {
		for (int i = 0; i < set.size(); i++) {
			if (!set.contains(i)) {
				return false;
			}
		}
		return true;
	}
}
