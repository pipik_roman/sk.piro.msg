/**
 * 
 */
package sk.piro.msgParser;

/**
 * Observer for reading properties file
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface PropertiesReaderObserver {
	/**
	 * Invoked when property with one value is found
	 * 
	 * @param key
	 *            property key
	 * @param value
	 *            property value
	 */
	public void nextProperty(String key, String value);
}
