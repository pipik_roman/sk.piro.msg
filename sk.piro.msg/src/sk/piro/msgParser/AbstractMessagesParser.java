package sk.piro.msgParser;

import sk.piro.msg.DuplicateMessageAction;
import sk.piro.msg.MutableMessages;

abstract public class AbstractMessagesParser<MessagesSource> extends MutableMessages {
	/**
	 * Use default {@link DuplicateMessageAction#Throw}
	 */
	public AbstractMessagesParser() {
		this(DuplicateMessageAction.Throw);
	}

	public AbstractMessagesParser(DuplicateMessageAction duplicateMessageAction) {
		super(duplicateMessageAction);
	}

	/**
	 * Parse all message sources with actual settings
	 * 
	 * @param sources
	 *            sources of messages
	 */
	abstract public void parse(MessagesSource... sources);
	/**
	 * Parse default message sources for specified locale. Default message sources are stored as properties files, so {@link PropertiesMessagesParser} may be created
	 * @param locale locale of default messages
	 */
	abstract public void parseDefault(String locale);
}
