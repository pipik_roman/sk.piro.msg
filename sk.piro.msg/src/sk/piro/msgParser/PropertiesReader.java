package sk.piro.msgParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Custom implementation of reader for .properties files.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class PropertiesReader {
	private final BufferedReader reader;
	private final StringBuilder value = new StringBuilder();
	private String key = null;
	private boolean nl = false;

	/**
	 * Create properties reader
	 * 
	 * @param input
	 *            input to read
	 */
	public PropertiesReader(InputStreamReader input) {
		this.reader = new BufferedReader(input);
	}

	private static boolean isComment(char start) {
		return start == '#' || start == '!';
	}

	private static boolean isNl(char end) {
		return end == '\\';
	}

	/**
	 * Read properties through observer
	 * 
	 * @param observer
	 *            observer to notify
	 * @throws IOException
	 *             if properties cannot be read properly
	 */
	public void process(PropertiesReaderObserver observer) throws IOException {
		try {
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					if (nl) {// ended with new line symbol but this line was last
						notify(observer);
					}
					break;
				}
				processLine(line, observer);
			}

		} finally {
			reader.close();
		}
	}

	private void processLine(String line, PropertiesReaderObserver observer) {
		if (line.isEmpty()) {
			notify(observer);
			return;
		}
		char start = line.charAt(0);
		if (isComment(start)) {
			return;
		}

		if (nl) {
			processRightPart(line, observer);
		} else {
			processSingleLine(line, observer);
		}
	}

	private void processSingleLine(String line, PropertiesReaderObserver observer) {
		int separator = -1;

		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);
			switch (c) {
			case '#':
			case '!':
				if (separator == -1) {// still not found
					line = line.substring(0, i);
				}
				break;
			case '=':
			case ':':
			case ' ':
				separator = i;
				break;
			default:
				break;
			}
			if (separator > -1) {
				break;
			}
		}

		String left;
		String right;

		if (separator == -1) {
			left = line;
			right = "";
		} else {
			left = line.substring(0, separator);
			right = line.substring(separator + 1, line.length());
		}
		processLeftPart(left);
		processRightPart(right, observer);
	}

	private void processLeftPart(String text) {
		key = text;
	}

	private void processRightPart(String line, PropertiesReaderObserver observer) {
		if (line.isEmpty()) {
			nl = false;
		} else {
			char end = line.charAt(line.length() - 1);
			nl = isNl(end);
		}

		if (nl) {
			value.append(line.substring(0, line.length() - 1));
			value.append('\n');
		} else {
			value.append(line.substring(0, line.length()));
			notify(observer);
		}
	}

	private void notify(PropertiesReaderObserver observer) {
		if (key == null) {
			return;
		}
		observer.nextProperty(key, value.toString());
		key = null;
		value.delete(0, value.length());
		nl = false;
	}
}
