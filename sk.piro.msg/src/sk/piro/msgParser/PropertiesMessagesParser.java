package sk.piro.msgParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import sk.piro.msg.DuplicateMessageAction;
import sk.piro.msg.MessageException;

/**
 * Parser accepting any {@link InputStream} with (extended) properties file syntax
 * 
 * @author Ing. Pipik Roman - roman.pipik@dominanz.sk pipik.roman@gmail.com www.dominanz.sk
 * 
 */
public class PropertiesMessagesParser extends AbstractMessagesParser<InputStream> {
	private static MessageParser messageParser = new MessageParser();

	private static void processStream(InputStream stream, Charset charset, PropertiesReaderObserver observer) throws IOException {
		InputStreamReader reader = new InputStreamReader(stream, charset);
		try {
			new PropertiesReader(reader).process(observer);
		} catch (IOException e) {
			throw new MessageException(e);
		} finally {
			reader.close();
		}
	}

	private final PropertiesReaderObserver parsingObserver = new PropertiesReaderObserver() {
		@Override
		public void nextProperty(String key, String value) {
			add(key, messageParser.parse(value));
		}
	};

	private final Charset charset;

	/**
	 * Use default {@link DuplicateMessageAction#Throw} and UTF-8 charset
	 */
	public PropertiesMessagesParser() {
		this(DuplicateMessageAction.Throw, Charset.forName("UTF-8"));
	}

	/**
	 * Create parset wiht specified settings
	 * 
	 * @param duplicateMessageAction
	 *            default action for duplicate messages
	 * @param charset
	 *            charset
	 */
	public PropertiesMessagesParser(DuplicateMessageAction duplicateMessageAction, Charset charset) {
		super(duplicateMessageAction);
		this.charset = charset;
	}

	@Override
	public void parseDefault(String locale) {
		parse(PropertiesMessagesParser.class.getResourceAsStream("defaults.properties"));
		locale = locale.toLowerCase();
		parse(PropertiesMessagesParser.class.getResourceAsStream("defaults_"+locale+".properties"));
	}
	@Override
	public void parse(InputStream... streams) {
		if (streams == null || streams.length == 0) {
			return;
		}
		try {
			for (InputStream s : streams) {
				processStream(s, charset, parsingObserver);
			}
		} catch (IOException e) {
			throw new MessageException(e);
		}
	}

	public Charset getCharset() {
		return charset;
	}

}
